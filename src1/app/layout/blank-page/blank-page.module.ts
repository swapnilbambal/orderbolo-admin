import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BlankPageRoutingModule } from './blank-page-routing.module';
import { BlankPageComponent } from './blank-page.component';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [CommonModule, BlankPageRoutingModule, NgbModule, FormsModule, ReactiveFormsModule],
    declarations: [BlankPageComponent]
})
export class BlankPageModule {}
