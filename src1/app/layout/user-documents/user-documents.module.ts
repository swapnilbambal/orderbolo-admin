import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { UserDocumentsRoutingModule } from './user-documents-routing.module';
import { UserDocumentsComponent } from './user-documents.component';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [CommonModule, UserDocumentsRoutingModule, NgbModule, FormsModule, ReactiveFormsModule],
    declarations: [UserDocumentsComponent]
})
export class UserDocumentsModule {}
