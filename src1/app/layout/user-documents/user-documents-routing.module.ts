import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserDocumentsComponent } from './user-documents.component';

const routes: Routes = [
    {
        path: '',
        component: UserDocumentsComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UserDocumentsRoutingModule {}
