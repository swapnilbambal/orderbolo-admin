import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CustomerReviewsRoutingModule } from './customer-reviews-routing.module';
import { CustomerReviewsComponent } from './customer-reviews.component';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [CommonModule, CustomerReviewsRoutingModule, NgbModule, FormsModule, ReactiveFormsModule],
    declarations: [CustomerReviewsComponent]
})
export class CustomerReviewsModule {}
