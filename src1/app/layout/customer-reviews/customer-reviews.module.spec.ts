import { CustomerReviewsModule } from './customer-reviews.module';

describe('CustomerReviewsModule', () => {
    let customerReviewsModule: CustomerReviewsModule;

    beforeEach(() => {
        customerReviewsModule = new CustomerReviewsModule();
    });

    it('should create an instance', () => {
        expect(customerReviewsModule).toBeTruthy();
    });
});
