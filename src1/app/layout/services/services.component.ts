import { Component, OnInit, ElementRef  } from '@angular/core';
import { Http, Response, RequestOptions, URLSearchParams  } from '@angular/http';
import {  ActivatedRoute, Router } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
// services
import { GeneralService } from '../../services/general.service';

@Component({
    selector: 'app-services',
    templateUrl: './services.component.html',
    styleUrls: ['./services.component.scss']
})
export class ServicesComponent implements OnInit {
    serviceRespnose: any;
    serviceList: any;
    imgUrl = 'http://vitsolutions24x7.com/orderbolo/api/api/assets/uploads/category_imgs/';
    ServicesimgUrl = 'http://vitsolutions24x7.com/orderbolo/api/api/assets/uploads/services_imgs/';
            //   http://vitsolutions24x7.com/orderbolo/api/api/assets/uploads/category_imgs/massage_and_fitness.jpeg
    closeResult: string;
    formTitle: string;
    category: any = {name: '', description: ''};
    files: any;
    modalReference: NgbModalRef;

    constructor(
        public http: Http,
        private modalService: NgbModal,
        private router: Router,
        private route: ActivatedRoute,
        public elem: ElementRef,
        private GeneralService: GeneralService) {
    }
    onSelectedFile(event) {
        this.files = event.target.files;
          console.log(this.files);
        }

    ngOnInit() {
        this.getServiceList();
    }


    open(content) {
        this.formTitle = 'New Category';
        this.modalReference = this.modalService.open(content);
        this.modalReference.result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    public getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }
// list of all services
    getServiceList() {
        const sampleUrl = 'http://vitsolutions24x7.com/orderbolo/api/api/ProjectController/getServiceList';
        this.http.get(sampleUrl)
        .subscribe((response) => {
            this.serviceRespnose = response.json();
            this.serviceList = this.serviceRespnose.response.servicesData;
        });
    }

// delete service by ID
deleteByID(id) {
    alert(id);
}


}
