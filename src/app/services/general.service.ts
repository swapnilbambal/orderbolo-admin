import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, URLSearchParams  } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable({
  providedIn: 'root'
})
export class GeneralService {
  Url: any = 'http://vitsolutions24x7.com/orderbolo/api/api/ProjectController/';

  constructor(private http: Http) {}

  public insertData(formdata: any ) {

    const _url: string = this.Url + 'addCategory';
    return this.http.post(_url, formdata)
    .catch(this._errorHandler) ;

  }

  
  public insertRequiredDocs(formdata: any ) {

    const _url: string = this.Url + 'DC_addRequiredDocuments';
    return this.http.post(_url, formdata)
    .catch(this._errorHandler) ;

  }

  //document doctor category add
  public DCAddDocument(formdata: any ) {
    const _url: string = this.Url + 'DC_addCategory';
    return this.http.post(_url, formdata)
    .catch(this._errorHandler) ;
  }

  
  public addCategory(formdata: any ) {
    const _url: string = this.Url + 'addCategory';
    return this.http.post(_url, formdata)
    .catch(this._errorHandler) ;
  }
   public deleteSubCategoryById(data){
    
      let url: string = this.Url+'deleteSubCategoryById';
      return this.http.post(url, data)
      .map((res:Response) => res);
  }

  public addSubCategory(formdata: any ) {
    const _url: string = this.Url + 'addSubCategory';
    return this.http.post(_url, formdata)
    .catch(this._errorHandler) ;
  }

  
  private _errorHandler(error: Response) {

      console.error('Error Occured: ' + error);
      return Observable.throw(error || 'Some Error on Server Occured');
  }


  //   public  getFilesList(id:string) {
  //     let data = {id:id}
  //     let _url: string = this.Url+'sub_folder_files.php';
  //     return this.http.get(_url, {params: data});
  //   }

  // delete record with attachment
  public  deleteRecordByFile(id: string) {
    const data = {id: id};
    const _url: string = this.Url + 'delete_sub_folder_single_file.php';
    return this.http.get(_url, {params: data});
  }

  // delete record without attachment
  public  deleteRecordById(id: string) {
    const data = {id: id};
    const _url: string = this.Url + 'delete_sub_folder_single_file.php';
    return this.http.get(_url, {params: data});
  }


  public deleteUserById(data){
      let url: string = this.Url+'deleteUserById';
      return this.http.post(url, data)
      .map((res:Response) => res);
  }


   public deleteServiceById(data){
      let url: string = this.Url+'deleteServiceById';
      return this.http.post(url, data)
      .map((res:Response) => res);
  }

   public deleteCategoryById(data){
      let url: string = this.Url+'deleteCategoryById';
      return this.http.post(url, data)
      .map((res:Response) => res);
  }
  

  public addUserData(data:any) {
    
        let url:string = this.Url+'userCommonFunction';
        return this.http.post(url, data)
        .map((result:Response) => result);

  }

   public addService(data:any) {
    
        let url:string = this.Url+'addService';
        return this.http.post(url, data)
        .map((result:Response) => result);

  }

  public getSubCategoryById(data:any) {

    let url:string = this.Url+'getSubCategoryById';
        return this.http.post(url, data)
        .map((result:Response) => result);

  }




}
