import { Component, OnInit, ElementRef  } from '@angular/core';
import { Http, Response, RequestOptions, URLSearchParams  } from '@angular/http';
import {  ActivatedRoute, Router } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
// services
import { GeneralService } from '../../services/general.service';

@Component({
    selector: 'app-document-master',
    templateUrl: './document-master.component.html',
    styleUrls: ['./document-master.component.scss']
})
export class DocumentMasterComponent implements OnInit {
    DCUrl = 'http://vitsolutions24x7.com/orderbolo/api/api/assets/document_doctor/category/';
    closeResult: string;
    formTitle: string;
    documents: any = {name: ''};
    files: any;
    documentList: any;
    documentsData: any;
    constructor(
        public http: Http,
        private modalService: NgbModal,
        private router: Router,
        private route: ActivatedRoute,
        public elem: ElementRef,
        private GeneralService: GeneralService) {
    }
    onSelectedFile(event) {
        this.files = event.target.files;
          console.log(this.files);
        }

    ngOnInit() {
        this.getDocList();
    }


    open(content) {
        this.formTitle = 'Document Master';
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    public getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }

    addDocument() {
    const files = this.files;
    const formData = new FormData();
    formData.append('user_id', '1');
    
    formData.append('user_id', '1');
    formData.append('access_token', 'vbJ9IjocNQT8MjM5MWxaLXJaIjogMQF9NQIaOQZ3M7MbDG4awQAzwD8NymlIz3TaB3hew70AREwoTaSlTCJox7A8yHpbBlSUQjAN');
    for (let j = 0; j < files.length; j++) {
            formData.append('file', files[j], files[j].name);
    }
    this.GeneralService.DCAddDocument(formData).subscribe(res => {
        if (res.json().status === 'success') {
            this.getDocList();
        }
      });
    }

    getDocList() {
        const sampleUrl = 'http://vitsolutions24x7.com/orderbolo/api/api/ProjectController/DC_categoryList';
        this.http.get(sampleUrl)
        .subscribe((response) => {
            this.documentList = response.json();
            this.documentsData = this.documentList.response.categories;
        });

    }


}
