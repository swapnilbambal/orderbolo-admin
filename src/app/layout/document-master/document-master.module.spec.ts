import { DocumentMasterModule } from './document-master.module';

describe('DocumentMasterModule', () => {
    let documentMasterModule: DocumentMasterModule;

    beforeEach(() => {
        documentMasterModule = new DocumentMasterModule();
    });

    it('should create an instance', () => {
        expect(documentMasterModule).toBeTruthy();
    });
});
