import { Component, OnInit, ElementRef  } from '@angular/core';
import { Http, Response, RequestOptions, URLSearchParams  } from '@angular/http';
import {  ActivatedRoute, Router } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
// services
import { GeneralService } from '../../services/general.service';

@Component({
    selector: 'app-users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
    userResponse: any;  
    userLists: any;
    profileImgUrl = 'http://vitsolutions24x7.com/orderbolo/api/api/assets/uploads/users_imgs/';
    closeResult: string;
    formTitle: string;
    userData: any = {username: '', mobile: ''};
    files: any;
    modalReference: NgbModalRef;

    constructor(
        public http: Http,
        private modalService: NgbModal,
        private router: Router,
        private route: ActivatedRoute,
        public elem: ElementRef,
        private GeneralService: GeneralService) {}

    onSelectedFile(event) {
        this.files = event.target.files;
          console.log(this.files);
        }

    ngOnInit() {
        this.getuserLists();
    }


    open(content) {
        this.formTitle = 'Add User';
        this.modalReference = this.modalService.open(content);
        this.modalReference.result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    public getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }

    // delete users list by id
    deleteUser(id) {
        // alert(id);
        let formData = new FormData();
        formData.append('id', id);
        this.GeneralService.deleteUserById(formData).subscribe(res => {
          console.log(res);
         //sthis.results = res.json();
          this.getuserLists();
        });
    }
    // get all users list
    getuserLists() {
        const sampleUrl = 'http://vitsolutions24x7.com/orderbolo/api/api/ProjectController/getAllUsers';
        this.http.get(sampleUrl)
        .subscribe((response) => {
            this.userResponse = response.json();
            this.userLists = this.userResponse.response;
        });
    }

    //add user
    addUser(){

        const files = this.files;
        const formData = new FormData();
        formData.append('user_id', '1');
        formData.append('access_token', 'vbJ9IjocNQT8MjM5MWxaLXJaIjogMQF9NQIaOQZ3M7MbDG4awQAzwD8NymlIz3TaB3hew70AREwoTaSlTCJox7A8yHpbBlSUQjAN');
        formData.append('username', this.userData.username);
        formData.append('phone', this.userData.mobile);
        formData.append('action', "add");
       // formData.append('description', this.category.description);
        for (let j = 0; j < files.length; j++) {
                formData.append('file', files[j], files[j].name);
        }
        this.GeneralService.addUserData(formData).subscribe(res => {
    
            if (res.json().status === 'success') {
                this.getuserLists();                
                this.modalReference.close();
            }
          });
    }

}
