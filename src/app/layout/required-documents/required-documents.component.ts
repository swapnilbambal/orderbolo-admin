import { Component, OnInit, ElementRef  } from '@angular/core';
import { Http, Response, RequestOptions, URLSearchParams  } from '@angular/http';
import {  ActivatedRoute, Router } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, FormArray, FormControl } from '@angular/forms';
// services
import { GeneralService } from '../../services/general.service';

@Component({
    selector: 'app-required-documents',
    templateUrl: './required-documents.component.html',
    styleUrls: ['./required-documents.component.scss']
})
export class RequiredDocumentsComponent implements OnInit {
    imgUrl = 'http://vitsolutions24x7.com/orderbolo/api/api/assets/uploads/category_imgs/';
    closeResult: string;
    formTitle: string;
    document: any = {id: ''};
    files: any;
    docList: any;
    docListData: any;
    docSubList: any;
    docSubListData: any;
    checkboxList: FormGroup;
    dataCheck: any;
    constructor(
        private fb: FormBuilder,
        public http: Http,
        private modalService: NgbModal,
        private router: Router,
        private route: ActivatedRoute,
        public elem: ElementRef,
        private GeneralService: GeneralService) {
    }
    onSelectedFile(event) {
        this.files = event.target.files;
          console.log(this.files);
        }

    ngOnInit() {
        this.getDocList();
        this.getSubDocList();
        this.checkboxList = this.fb.group({
            useremail: this.fb.array([])
          });
        this.dataCheck = this.checkboxList;
        alert(JSON.stringify(this.dataCheck));
    }

    onChange(id: string, isChecked: boolean) {
        const emailFormArray = <FormArray>this.checkboxList.controls.useremail;
    
        if (isChecked) {
          emailFormArray.push(new FormControl(id));
        } else {
          let index = emailFormArray.controls.findIndex(x => x.value == id)
          emailFormArray.removeAt(index);
        }
      }


    open(content) {
        this.formTitle = 'Required Document For Master Documents';
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    public getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }

    addDocument() {
    // const files = this.files;
    const formData = new FormData();
    formData.append('user_id', '1');
    formData.append('document_id', this.document.id);
    formData.append('required_id', this.dataCheck);
    formData.append('access_token', 'vbJ9IjocNQT8MjM5MWxaLXJaIjogMQF9NQIaOQZ3M7MbDG4awQAzwD8NymlIz3TaB3hew70AREwoTaSlTCJox7A8yHpbBlSUQjAN');
    // for (let j = 0; j < files.length; j++) {
    //         formData.append('file', files[j], files[j].name);
    // }
    this.GeneralService.insertRequiredDocs(formData).subscribe(res => {
        if (res.json().status === 'success') {
            this.getDocList();
            this.getSubDocList();
        }
      });
    }

    getDocList() {
        const sampleUrl = 'http://vitsolutions24x7.com/orderbolo/api/api/ProjectController/DC_categoryList';
        this.http.get(sampleUrl)
        .subscribe((response) => {
            this.docList = response.json();
            this.docListData = this.docList.response.categories;
        });
    }

    
    getSubDocList() {
        const sampleUrl = 'http://vitsolutions24x7.com/orderbolo/api/api/ProjectController/DC_categoryDocumentList';
        this.http.get(sampleUrl)
        .subscribe((response) => {
            this.docSubList = response.json();
            this.docSubListData = this.docSubList.response.categories;
        });
    }


}
