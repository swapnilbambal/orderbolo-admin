import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RequiredDocumentsComponent } from './required-documents.component';

const routes: Routes = [
    {
        path: '',
        component: RequiredDocumentsComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RequiredDocumentsRoutingModule {}
