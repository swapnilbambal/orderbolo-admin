import { RequiredDocumentsModule } from './required-documents.module';

describe('UserDocumentsModule', () => {
    let requiredDocumentsModule: RequiredDocumentsModule;

    beforeEach(() => {
        requiredDocumentsModule = new RequiredDocumentsModule();
    });

    it('should create an instance', () => {
        expect(requiredDocumentsModule).toBeTruthy();
    });
});
