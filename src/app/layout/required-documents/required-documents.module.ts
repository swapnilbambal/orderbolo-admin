import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RequiredDocumentsRoutingModule } from './required-documents-routing.module';
import { RequiredDocumentsComponent } from './required-documents.component';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [CommonModule, RequiredDocumentsRoutingModule, NgbModule, FormsModule, ReactiveFormsModule],
    declarations: [RequiredDocumentsComponent]
})
export class RequiredDocumentsModule {}
