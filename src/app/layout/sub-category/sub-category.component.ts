import { Component, OnInit, ElementRef  } from '@angular/core';
import { Http, Response, RequestOptions, URLSearchParams  } from '@angular/http';
import {  ActivatedRoute, Router } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
// services
import { GeneralService } from '../../services/general.service';

@Component({
    selector: 'app-sub-category',
    templateUrl: './sub-category.component.html',
    styleUrls: ['./sub-category.component.scss']
})
export class SubCategoryComponent implements OnInit {
    category_id:any;
    categoryList1: any;  categoryList: any;
    imgUrl = 'http://vitsolutions24x7.com/orderbolo/api/api/assets/uploads/sub_category_imgs/';
    closeResult: string;
    formTitle: string;
    category: any = {name: '', description: ''};
    files: any;
    modalReference: NgbModalRef;
    subcategoryList1: any;

    constructor(
        public http: Http,
        private modalService: NgbModal,
        private router: Router,
        private route: ActivatedRoute,
        public elem: ElementRef,
        private GeneralService: GeneralService) {
    }
    onSelectedFile(event) {
        this.files = event.target.files;
          console.log(this.files);
        }

    ngOnInit() {
        this.getSubCategoryList();
        this.getCategorylist();
    }


    open(content) {
        this.formTitle = 'New Sub Category';
        this.modalReference = this.modalService.open(content);
        this.modalReference.result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    public getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }

    optionVal(){
       this.category_id = this.category_id;
    }
    addSubCategory() {
        // alert(this.category_id);
        const files = this.files;
        const formData = new FormData();
        formData.append('user_id', '1');
        formData.append('access_token', 'vbJ9IjocNQT8MjM5MWxaLXJaIjogMQF9NQIaOQZ3M7MbDG4awQAzwD8NymlIz3TaB3hew70AREwoTaSlTCJox7A8yHpbBlSUQjAN');
        formData.append('name', this.category.name);
        formData.append('category_id', this.category_id);
        for (let j = 0; j < files.length; j++) {
                formData.append('file', files[j], files[j].name);
        }
        this.GeneralService.addSubCategory(formData).subscribe(res => {
            if (res.json().status === 'success') {
                this.getSubCategoryList();
                this.modalReference.close();
            }
        });

    }

   
    getSubCategoryList() {
        const sampleUrl = 'http://vitsolutions24x7.com/orderbolo/api/api/ProjectController/getSubCategoryList';
        this.http.get(sampleUrl)
        .subscribe((response) => {

            this.categoryList1 = response.json();
            this.categoryList = this.categoryList1.response.categories;
        });

    }

     
    getCategorylist() {
        const sampleUrl = 'http://vitsolutions24x7.com/orderbolo/api/api/ProjectController/categorylist';
        this.http.get(sampleUrl)
        .subscribe((response) => {
            this.subcategoryList1 = response.json();
            this.subcategoryList1 = this.subcategoryList1.response.categories;
            // console.log(">>>>>>>>>>"+this.subcategoryList1);
        });

    }
categorylist

     // delete service by ID
     deleteByID(id) {
        //  alert(id);
        let data = new FormData();
        data.append('id', id);
        this.GeneralService.deleteSubCategoryById(data).subscribe(res=>{
            console.log(res.json());
            this.getSubCategoryList();
        })
    }

}
