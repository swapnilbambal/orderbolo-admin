import { Component, OnInit, ElementRef  } from '@angular/core';
import { Http, Response, RequestOptions, URLSearchParams  } from '@angular/http';
import {  ActivatedRoute, Router } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
// services
import { GeneralService } from '../../services/general.service';

@Component({
    selector: 'app-orders',
    templateUrl: './orders.component.html',
    styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {
    categoryimgUrl = 'http://vitsolutions24x7.com/orderbolo/api/api/assets/uploads/category_imgs/';
    profileimgUrl = 'http://vitsolutions24x7.com/orderbolo/api/api/assets/uploads/users_imgs/';
    closeResult: string;
    formTitle: string;
    files: any;
    modalReference: NgbModalRef;
    orderList: any;
    orderData: any;

    constructor(
        public http: Http,
        private modalService: NgbModal,
        private router: Router,
        private route: ActivatedRoute,
        public elem: ElementRef,
        private GeneralService: GeneralService) {
    }
    onSelectedFile(event) {
        this.files = event.target.files;
          console.log(this.files);
        }

    ngOnInit() {
        this.getOrderList();
    }


    open(content) {
        this.formTitle = 'New Category';
        this.modalReference = this.modalService.open(content);
        this.modalReference.result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    public getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }

    addOrder() {
    const files = this.files;
    const formData = new FormData();
    formData.append('user_id', '1');
    formData.append('access_token', 'vbJ9IjocNQT8MjM5MWxaLXJaIjogMQF9NQIaOQZ3M7MbDG4awQAzwD8NymlIz3TaB3hew70AREwoTaSlTCJox7A8yHpbBlSUQjAN');
    for (let j = 0; j < files.length; j++) {
            formData.append('file', files[j], files[j].name);
    }
    this.GeneralService.insertData(formData).subscribe(res => {
        if (res.json().status === 'success') {
            this.getOrderList();
            this.modalReference.close();
        }
      });
    }

    getOrderList() {
        const sampleUrl = 'http://vitsolutions24x7.com/orderbolo/api/api/ProjectController/OrderList';
        this.http.get(sampleUrl)
        .subscribe((response) => {
            this.orderList = response.json();
            this.orderData = this.orderList.response.result.orderDetailsByUserId;
        });

    }


}
