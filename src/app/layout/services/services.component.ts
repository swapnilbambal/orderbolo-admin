import { Component, OnInit, ElementRef  } from '@angular/core';
import { Http, Response, RequestOptions, URLSearchParams  } from '@angular/http';
import {  ActivatedRoute, Router } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
// services
import { GeneralService } from '../../services/general.service';

@Component({
    selector: 'app-services',
    templateUrl: './services.component.html',
    styleUrls: ['./services.component.scss']
})
export class ServicesComponent implements OnInit 
{

    serviceRespnose: any;
    serviceList: any;
    imgUrl = 'http://vitsolutions24x7.com/orderbolo/api/api/assets/uploads/category_imgs/';
    ServicesimgUrl = 'http://vitsolutions24x7.com/orderbolo/api/api/assets/uploads/services_imgs/';
            //   http://vitsolutions24x7.com/orderbolo/api/api/assets/uploads/category_imgs/massage_and_fitness.jpeg
    closeResult: string;
    formTitle: string;
    service: any = {title: '', photo: '', price: '', description: '',category_id:'',subcategory_id:"", feature:""};
    nameList: any;
    subnameList: any;
    subdata:any;
    data:any; 
    nameId:Number; 
    files: any;
    modalReference: NgbModalRef;

    constructor(

        public http: Http,
        private modalService: NgbModal,
        private router: Router,
        private route: ActivatedRoute,
        public elem: ElementRef,
        private GeneralService: GeneralService) {

    }

    onSelectedFile(event) {
        this.files = event.target.files;
          console.log(this.files);
    }

    ngOnInit() {
        this.getServiceList();
    }

    open(content) {

        this.formTitle = 'New Category';
        this.modalReference = this.modalService.open(content);
        this.modalReference.result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });

    }

    public getDismissReason(reason: any): string {

        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }

    }
    // list of all services
    getServiceList() {
      
        const sampleUrl = 'http://vitsolutions24x7.com/orderbolo/api/api/ProjectController/getServiceList';
        this.http.get(sampleUrl)
        .subscribe((response) => {
            this.serviceRespnose = response.json();
            this.serviceList = this.serviceRespnose.response.servicesData;
            this.data =this.serviceRespnose.category;
             this.nameList= this.data;
            console.log(this.serviceRespnose.category);
        });
    }

    // delete service by ID
    deleteByID(id) {

        let data = new FormData();
        data.append('id', id);
        this.GeneralService.deleteServiceById(data).subscribe(res=>{
            console.log(res.json());
            this.getServiceList();
        });

    }

    addService() {

        const files = this.files;
        const formData = new FormData();
        formData.append('user_id', '1');
        formData.append('access_token', 'vbJ9IjocNQT8MjM5MWxaLXJaIjogMQF9NQIaOQZ3M7MbDG4awQAzwD8NymlIz3TaB3hew70AREwoTaSlTCJox7A8yHpbBlSUQjAN');
        formData.append('title', this.service.title);
        formData.append('price', this.service.price);
        formData.append('description', this.service.description);
        formData.append('phone', this.service.phone);
        formData.append('category_id',this.service.category_id);
        formData.append('subcategory_id',this.service.subcategory_id);
        formData.append('is_feature',this.service.feature);
        
        for (let j = 0; j < files.length; j++) {
                formData.append('file', files[j], files[j].name);
        }
        this.GeneralService.addService(formData).subscribe(res => {
            if (res.json().status === 'success') {
                this.getServiceList();
                this.modalReference.close();
            }
        });
    }

    selectName(id) {
            this.service.category_id=id;
            const formData = new FormData();
            formData.append("category_id",id);
            this.GeneralService.getSubCategoryById(formData).subscribe(res => {
                this.serviceRespnose = res.json();
            
            if (this.serviceRespnose.status === 'success') {

                this.subdata =this.serviceRespnose.response;
                this.subnameList= this.subdata;
                console.log(this.subnameList);
       
            }
        });

    }

    selectSubCatName(subcategory_id) {
        //  alert(subcategory_id);
         this.service.subcategory_id=subcategory_id;
    }
    

}  
